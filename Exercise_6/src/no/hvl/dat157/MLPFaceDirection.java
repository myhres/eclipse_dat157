package no.hvl.dat157;

import images.ImageConverter;
import no.patternsolutions.javann.Backpropagation;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;

public class MLPFaceDirection {
    private final static int RESOULUTION = 32*30;

    public static void main(String[] args) throws IOException {
        // Oppretter nn
        Backpropagation bp = new Backpropagation(RESOULUTION, new int[] { 50 }, 4);
        bp.setLearnRate(0.01);
        bp.setMomentumEnabled(true);
        bp.setMomentum(0.9);
        bp.randomizeWeights(-1.0, 1.0);
        bp.setIterations(10000);

        // Henter alle bildene i sine fire retninger
        // images[0] -> left, images[1] -> right, osv...
        double[][][] images = MLPFaceDirection.getImages("faces_32/", 128);
        int directions = 4;
        int totalImages = images[0].length * directions;

        // Training inputs: Holder alle bilder med størrelse RESOLUTION
        double[][] trainingInputs = new double[totalImages][RESOULUTION];

        // Hvert svar peker mot en retning/outputnode
        double[][] answers = new double[totalImages][directions];

        // Henter bilder og legger de i treningsdata
        int j = 0;
        for(int i = 0; i < totalImages; i++) {
            if(i % directions == 0) j++;

            trainingInputs[i] = images[i % directions][j-1];
            answers[i][i % directions] = 1.0;
        }

        // Trener nettet
        System.out.println("Training neural net...");
        bp.trainPatterns(trainingInputs, answers);

        // Tester alle bilder, og logger antall korrekte
        testImages(images, bp, "\nTesting images");

        // Tester ukjente bilder
        testImages(getImages("faces_32/unknown/", 12), bp, "\nTesting images");
    }

    /**
     * Tester bilder med et gitt bp-nett
     * @param images bilder som skal testes
     * @param bp nett som skal testes
     * @param message melding som vises før testresultat
     */
    private static void testImages(double[][][] images, Backpropagation bp, String message) {
        int directions = 4;
        int correct = 0;
        int totalImages = images[0].length * directions;
        double[] nodes;

        for(int i = 0; i < directions; i++) {
            for(double[] image : images[i]) {
                nodes = bp.run(image);

                if(maxIndex(nodes) == i) ++correct;
            }
        }

        NumberFormat formatter = new DecimalFormat("#0.000");
        double percent = ((double) correct / (double) totalImages) * 100.0;

        System.out.println("\n" + message);
        System.out.println("Correct: " + correct + "/" + totalImages
                + " (" + formatter.format(percent) + "%)");
    }


    // Finner den største verdien i en liste
    private static int maxIndex(double[] nodes) {
        double maxValue = 0.0;
        int maxIndex = 0;

        for(int i = 0; i < nodes.length; i++) {
            if(nodes[i] > maxValue)  {
                maxValue = nodes[i];
                maxIndex = i;
            }
        }

        return maxIndex;
    }

    /**
     * Henter bilder fra en mappe med undermapper left/ right/ straight/ og up/
     * @param directory Mappe bilder skal hentes fra
     * @param imageCount Antall bilder per mappe som skal hentes
     * @return Bilder skilt i directions
     * @throws NullPointerException
     * @throws IOException
     */
    private static double[][][] getImages(String directory, int imageCount) throws NullPointerException, IOException {
        // Henter mapper
        File left = new File(directory + "left");
        File right = new File(directory + "right");
        File straight = new File(directory + "straight");
        File up = new File(directory + "up");

        // Henter alle filer fra hver mappe
        File[] files_left = left.listFiles();
        File[] files_right = right.listFiles();
        File[] files_straight = straight.listFiles();
        File[] files_up = up.listFiles();

        // Liste som holder alle bilder
        double[][][] images = new double[4][imageCount][RESOULUTION];

        // Konverter hvert bilde til double array
        for(int i = 0; i < imageCount; i++) {
            images[0][i] = ImageConverter.loadDoubles(files_left[i].toString());
            images[1][i] = ImageConverter.loadDoubles(files_right[i].toString());
            images[2][i] = ImageConverter.loadDoubles(files_straight[i].toString());
            images[3][i] = ImageConverter.loadDoubles(files_up[i].toString());
        }

        return images;
    }
}
