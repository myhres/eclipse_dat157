package no.hvl.dat157;

import images.ImageConverter;
import no.hvl.dat157.utils.ImageUtils;
import no.patternsolutions.javann.Adaline;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;

public class AdalineImage {
    public static void run(int trainingIterations, int testIterations,
                           int noise, double learnRate) throws IOException {
        int inputs = 10 * 10; // One input for each pixel
        int outputs = 3; // Three outputs, where T = node 0, O = node 3, N = node 2

        // Oppretter et nytt Adaline
        Adaline nn = new Adaline(inputs, outputs);
        nn.setWeightsInit(0);
        nn.setIterations(trainingIterations);
        nn.setLearnRate(learnRate);

        // Laster bilder til streng med bools
        boolean[] image_T = ImageConverter.loadBools("T.png");
        boolean[] image_O = ImageConverter.loadBools("O.png");
        boolean[] image_N = ImageConverter.loadBools("N.png");

        // Legger bildene til som treningsinputs
        boolean[][] trainingInputs = new boolean[3][100];
        trainingInputs[0] = image_T;
        trainingInputs[1] = image_O;
        trainingInputs[2] = image_N;

        // Fasit til trening
        boolean[][] answers = new boolean[3][3];
        answers[0][0] = true;
        answers[1][1] = true;
        answers[2][2] = true;

        // Trener nett
        nn.trainPatterns(trainingInputs, answers);

        /*
        boolean[] answerNode = nn.run(image_T);
        System.out.println(answerNode[0]);
        System.out.println(answerNode[1]);
        System.out.println(answerNode[2]);
        */

        // Tester med hver bokstav gitt antall ganger
        System.out.println("Testing T with " + noise + "% noise");
        meassureCorrect(nn, image_T, noise, "T", testIterations);

        System.out.println("Testing O with " + noise + "% noise");
        meassureCorrect(nn, image_O, noise, "O", testIterations);

        System.out.println("Testing N with " + noise + "% noise");
        meassureCorrect(nn, image_N, noise, "N", testIterations);
    }

    /**
     * Tester nettet et gitt antall ganger, og skriver ut antall rett
     * @param trainedAdaline
     * @param input
     * @param noise
     * @param expected
     * @param iterations
     */
    private static void meassureCorrect(Adaline trainedAdaline,
                                        boolean[] input, int noise,
                                        String expected, int iterations) {
        int sumCorrect = 0;
        int answerIndex = 0;
        boolean[] answerNode;
        NumberFormat formatter = new DecimalFormat("#0.000");
        int sumNodes;

        switch (expected) {
            case "T":
                answerIndex = 0;
                break;
            case "O":
                answerIndex = 1;
                break;
            case "N":
                answerIndex = 2;
                break;
        }

        for(int i = 0; i < iterations; i++) {
            answerNode = trainedAdaline.run(ImageUtils.addNoise(input, noise));
            sumNodes = sumOutputNodes(answerNode);

            if(answerNode[answerIndex] && sumNodes == 1) sumCorrect++;
        }

        double percent = ((double) sumCorrect / (double) iterations) * 100.0;
        System.out.println("Correct: " + sumCorrect + "/" + iterations
                + " (" + formatter.format(percent) + "%)");
    }

    private static int sumOutputNodes(boolean[] answerNode) {
        int sum = 0;

        for(boolean bool : answerNode) {
            if(bool) ++sum;
        }

        return sum;
    }
}
