package no.hvl.dat157;

public class TestNN {
    public static void main(String[] args) {
        try {
            int training_iterations = 10000;
            int test_iterations = 10000;
            int noise = 20;
            double learnRate = 0.1;

            System.out.println("Tester Perceptron:");
            PerceptronImage.run(training_iterations, test_iterations, noise, learnRate);

            System.out.println();
            System.out.println("Tester Adaline:");
            AdalineImage.run(training_iterations, test_iterations, noise, learnRate);

            System.out.println();
            System.out.println("Tester MLP:");
            MLPImage.run(training_iterations, test_iterations, noise, learnRate);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
