package no.hvl.dat157;

import images.ImageConverter;
import no.hvl.dat157.utils.ImageUtils;
import no.patternsolutions.javann.Backpropagation;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;

public class MLPImage {
    public static void run(int trainingIterations, int testIterations,
                           int noise, double learnRate) throws IOException {
        int inputs = 10 * 10;
        int outputs = 3;
        int[] hiddenLayers = { 10 };

        Backpropagation nn = new Backpropagation(inputs, hiddenLayers, outputs);
        nn.setLearnRate(learnRate);
        nn.setMomentumEnabled(true);
        nn.setMomentum(0.9);
        nn.randomizeWeights(-1.0, 1.0);
        nn.setIterations(trainingIterations);

        // Laster bilder til streng med bools
        double[] image_T = ImageConverter.loadDoubles("T.png");
        double[] image_O = ImageConverter.loadDoubles("O.png");
        double[] image_N = ImageConverter.loadDoubles("N.png");

        // Legger bildene til som treningsinputs
        double[][] trainingInputs = new double[3][100];
        trainingInputs[0] = image_T;
        trainingInputs[1] = image_O;
        trainingInputs[2] = image_N;

        // Fasit til trening
        double[][] answers = new double[3][3];
        answers[0][0] = 1.0;
        answers[1][1] = 1.0;
        answers[2][2] = 1.0;

        // Trener nett
        nn.trainPatterns(trainingInputs, answers);

        // Tester med hver bokstav gitt antall ganger
        System.out.println("Testing T with " + noise + "% noise");
        meassureCorrect(nn, image_T, noise, "T", testIterations);

        System.out.println("Testing O with " + noise + "% noise");
        meassureCorrect(nn, image_O, noise, "O", testIterations);

        System.out.println("Testing N with " + noise + "% noise");
        meassureCorrect(nn, image_N, noise, "N", testIterations);
    }

    /**
     * Tester nettet et gitt antall ganger, og skriver ut antall rett
     * @param trainedMLP NN to be tested
     * @param input input to test against NN
     * @param noise % of noies in input
     * @param expected expected value from NN
     * @param iterations amount of tests
     */
    private static void meassureCorrect(Backpropagation trainedMLP,
                                        double[] input, int noise,
                                        String expected, int iterations) {
        int sumCorrect = 0;
        int answerIndex = 0;
        double[] answerNode;
        NumberFormat formatter = new DecimalFormat("#0.000");

        switch (expected) {
            case "T":
                answerIndex = 0;
                break;
            case "O":
                answerIndex = 1;
                break;
            case "N":
                answerIndex = 2;
                break;
        }

        for(int i = 0; i < iterations; i++) {
            answerNode = trainedMLP.run(ImageUtils.addNoise(input, noise));
            if(answerIndex == maxIndex(answerNode)) sumCorrect++; // Winner takes all
        }

        double percent = ((double) sumCorrect / (double) iterations) * 100.0;
        System.out.println("Correct: " + sumCorrect + "/" + iterations
                + " (" + formatter.format(percent) + "%)");
    }

    private static int maxIndex(double[] nodes) {
        double maxValue = 0.0;
        int maxIndex = 0;

        for(int i = 0; i < nodes.length; i++) {
            if(nodes[i] > maxValue)  {
                maxValue = nodes[i];
                maxIndex = i;
            }
        }

        return maxIndex;
    }
}
