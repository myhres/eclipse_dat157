package no.hvl.dat157.utils;

import java.util.Random;

public class ImageUtils {
    /**
     * Leser inn et bilde og legger til støy
     * @param image bilde som blir introdusert til støy
     * @param noisePercent prosent antall støy (0 - 100)
     * @return et bilde med støy
     */
    public static boolean[] addNoise(boolean[] image, int noisePercent) {
        Random r = new Random();
        boolean[] newImage = new boolean[image.length];

        for(int i = 0; i < image.length; i++) {
            // noisePercent sjanse
            if((r.nextInt(100) + 1) <= noisePercent) {
                newImage[i] = !image[i];
            } else {
                newImage[i] = image[i];
            }
        }

        return newImage;
    }

    public static double[] addNoise(double[] image, int noisePercent) {
        Random r = new Random();
        double[] newImage = new double[image.length];

        for(int i = 0; i < image.length; i++) {
            // noisePercent sjanse
            if((r.nextInt(100) + 1) <= noisePercent) {
                newImage[i] = 1.0 - image[i];
            } else {
                newImage[i] = image[i];
            }
        }

        return newImage;
    }

    /**
     * Skriver ut et bilde til konsoll (10px*10px bilde)
     * @param image bilde som skal skrives ut
     */
    public static void printImage(boolean[] image) {
        for(int i = 0; i < 10; i++) {
            for(int j = 0; j < 10; j++) {
                if(image[i*10 + j]) System.out.print("_");
                else System.out.print("#");
            }

            System.out.println();
        }
    }

    public static void printImage(double[] image) {
        for(int i = 0; i < 10; i++) {
            for(int j = 0; j < 10; j++) {
                if(image[i*10 + j] == 1.0) System.out.print("_");
                else System.out.print("#");
            }

            System.out.println();
        }
    }
}
