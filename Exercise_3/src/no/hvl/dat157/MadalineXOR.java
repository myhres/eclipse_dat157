package no.hvl.dat157;

import no.patternsolutions.javann.Adaline;

public class MadalineXOR {
    public static void main(String[] args) {
        double learnRate = 0.1;
        int weigthsInit = 0;
        int iterations = 100;

        int inputs = 2; // Two inputs: x1 and x2
        int outputs = 1;

        // Oppretter adaline 1
        Adaline adaline1 = new Adaline(inputs, outputs);
        adaline1.setLearnRate(learnRate);
        adaline1.setWeightsInit(weigthsInit);
        adaline1.setIterations(iterations);

        // Oppretter adaline 2
        Adaline adaline2 = new Adaline(inputs, outputs);
        adaline2.setLearnRate(learnRate);
        adaline2.setWeightsInit(weigthsInit);
        adaline2.setIterations(iterations);

        // Oppretter madaline enhet
        Adaline madaline = new Adaline(inputs, outputs);
        madaline.setLearnRate(learnRate);
        madaline.setWeightsInit(weigthsInit);
        madaline.setIterations(iterations);

        // Oppretter treningsinput
        // (Every scenario: [f,f] [f,t] [t,f] [t,t])
        boolean[][] trainingInputs = new boolean[4][2];
        trainingInputs[1][1] = true;
        trainingInputs[2][0] = true;
        trainingInputs[3][0] = true;
        trainingInputs[3][1] = true;

        // Trener OR for adaline 1
        boolean[][] ada1Answer = new boolean[4][1];
        ada1Answer[0][0] = false;
        ada1Answer[1][0] = true;
        ada1Answer[2][0] = true;
        ada1Answer[3][0] = true;
        adaline1.trainPatterns(trainingInputs, ada1Answer);

        // Trener NAND for adaline 2
        boolean[][] ada2Answer = new boolean[4][1];
        ada2Answer[0][0] = true;
        ada2Answer[1][0] = true;
        ada2Answer[2][0] = true;
        ada2Answer[3][0] = false;
        adaline2.trainPatterns(trainingInputs, ada2Answer);

        // Trener AND for madaline
        boolean[][] madaAnswer = new boolean[4][1];
        madaAnswer[0][0] = false;
        madaAnswer[1][0] = false;
        madaAnswer[2][0] = false;
        madaAnswer[3][0] = true;
        madaline.trainPatterns(trainingInputs, madaAnswer);

        // Testdata (Samme som trainingInputs)
        boolean[][] patterns = {
                { false, false },
                { false, true },
                { true, false },
                { true, true }
        };

        // Tester alle testdata
        for (boolean[] pattern : patterns) {
            System.out.print(pattern[0] + " XOR " + pattern[1] + ": ");

            // Kjører Madaline med adaline1 og adaline2 som input
            System.out.println(
                    madaline.run(new boolean[] {
                            (adaline1.run(pattern))[0],
                            (adaline2.run(pattern))[0]
                    })[0]
            );
        }
    }
}
